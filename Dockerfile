FROM couchbase/server:community-6.5.0

COPY scripts/configure-node.sh /etc/service/config-couchbase/run
RUN chown -R couchbase:couchbase /etc/service
COPY scripts/create-sample-values.txt /opt/couchbase
