# Couchbase Sandbox

**This repository is a Fork of https://github.com/couchbase/server-sandbox**

Couchbase is pretty hard to run on a simple compose file so, i created a modified docker image to use it in a compose file easily. Related project is;  [ultimateai-challenge](https://gitlab.com/gulteking/ultimateai-challenge) .

## Image basically does;
 - Sets up couchbase  ce 6.5.0  with a bucket named `intent`
 - Adds GSI index on `intent` bucket 
 - Adds 5 sample documents into `intent` bucket.
 - Sets `Administrator` and `password` as username and password. 
  
## Usage in Docker-compose
```
version: "3.4"  
services:  
  couchbase:  
    image: gulteking/couchbase-sandbox:community-6.5.0  
    restart: on-failure  
    volumes:  
      - couchbase_data:/opt/couchbase/var  
    networks:  
      - ultimatenet  
    ports:  
      - 8091:8091  
      - 8092:8092  
      - 8093:8093  
      - 8094:8094  
      - 11210:11210  
      - 11207:11207  
volumes:  
  couchbase_data:  
networks:  
  ultimatenet:  
    driver: bridge
```
You ll be able to enter admin interface from `http://localhost:8091`. 
Credentials; `Administrator` and `password`

## Docker Hub 
hub.docker.com/repository/docker/gulteking/couchbase-sandbox/